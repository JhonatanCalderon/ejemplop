/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author SEDE LA PERLA
 */
public class Participante implements Comparable{
    String nombre;
    String cedula;
    String usuario;
    String clave;

    public Participante(String nombre, String cedula, String usuario, String clave) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.usuario = usuario;
        this.clave = clave;
    }

    public Participante(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }
    

    public Participante() {
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public int compareTo(Object o) {
        Participante x=(Participante)o;
       
     return ((int)(this.usuario.hashCode()-x.usuario.hashCode()));
    }
    
    
}
